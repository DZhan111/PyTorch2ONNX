import torch
from torch import nn
from torchvision.models import feature_extraction as FE 
import  pickle
from nnunet.training.model_restore import load_model_and_checkpoint_files
import os
import numpy as np
 
if __name__ == "__main__":
 
    #convert .model to .onnx
    Model_path = "/home/dzhan111@na.jnj.com/nnUNet_trained_models/nnUNet/3d_fullres/Task502_aurislundnodule100/nnUNetTrainerV2__nnUNetPlansv2.1"
    folds='all'
    checkpoint_name = "model_best" 
 
    trainer, params = load_model_and_checkpoint_files(Model_path, folds=folds, mixed_precision=True, checkpoint_name=checkpoint_name)  
    net = trainer.network
   
    checkpoint = torch.load(os.path.join( Model_path , folds, checkpoint_name +".model"))
 
    net.load_state_dict(checkpoint['state_dict'])
    net.eval()

    dummy_input = torch.randn(1, 1, 128, 128, 128).to("cuda")
    torch.onnx.export(
        net,
        dummy_input,
        'nnunet_tensorrt.onnx',
        export_params=True,
        do_constant_folding=True,
        input_names=['x'],
        output_names=['seg_outputs.4'],
        # dynamic_axes = {'x': {0: 'batch_size'},'seg_outputs.4': {0: 'batch_size'}}
        )

    model = net
    train_nodes, eval_nodes = FE.get_graph_node_names(model)
    print(train_nodes)