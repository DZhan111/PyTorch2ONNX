This repo includes the Python scripts to convert PyTorch model to ONNX model. 

It is worth noticing:

1. If the input dimension is fixed, a `dummy_input` is usually required. This project fixed the input dimension to 128x128x128 to keep the dimension the same as the training size.

2. If the input dimension is not fixed, uncomment the `dynamic_axes` in `torch.onnx.export` ([line 33](https://gitlab.com/DZhan111/PyTorch2ONNX/-/blob/main/nnunetmodel2onnx.py?ref_type=heads#L33)) and modified the corresponding TensorRT codes (not implemented in this project).

2. To find the correct nodes name, you can use `train_nodes, eval_nodes = FE.get_graph_node_names(model)` ([line 37](https://gitlab.com/DZhan111/PyTorch2ONNX/-/blob/main/nnunetmodel2onnx.py?ref_type=heads#L37)). Or it is also recommended to use software such as [Netron](https://netron.app/) to figure out the correct input/output nodes name.

3. Do not forget to `load_state_dict`.
