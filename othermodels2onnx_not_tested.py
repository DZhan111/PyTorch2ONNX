import torch.onnx 
import sys
sys.path.append('/home/dzhan111@na.jnj.com/Nodule_Segmentation/networks/CBIM-Medical-Image-Segmentation-main/')
from model.dim3.medformer import MedFormer

#Function to Convert to ONNX 
def Convert_ONNX(): 

    # set the model to inference mode 
    model.eval() 

    # Let's create a dummy input tensor  
    dummy_input = torch.randn(1, 1, 128, 128, 128, requires_grad=True) 

    # Export the model   
    torch.onnx.export(model,         # model being run 
         dummy_input,       # model input (or a tuple for multiple inputs) 
         "ImageClassifier.onnx",       # where to save the model  
         export_params=True,  # store the trained parameter weights inside the model file 
         opset_version=10,    # the ONNX version to export the model to 
         do_constant_folding=True,  # whether to execute constant folding for optimization 
         input_names = ['modelInput'],   # the model's input names 
         output_names = ['modelOutput'], # the model's output names 
         dynamic_axes={'modelInput' : {0 : 'batch_size'},    # variable length axes 
                                'modelOutput' : {0 : 'batch_size'}}) 
    print(" ") 
    print('Model has been converted to ONNX')


if __name__ == "__main__": 


    path = "/home/dzhan111@na.jnj.com/Nodule_Segmentation/networks/CBIM-Medical-Image-Segmentation-main/exp/auris/auris_3d_medformer/fold_0_best.pth" 
    model = MedFormer(1, 3, 32, map_size=[4,4,4], conv_block='BasicBlock', conv_num=[2,0,0,0, 0,0,2,2], trans_num=[0,2,4,6, 4,2,0,0], num_heads=[1,1,1,1, 1,1,1,1], fusion_depth=2, fusion_dim=320, fusion_heads=10, expansion=4, attn_drop=0., proj_drop=0., proj_type='depthwise',   kernel_size=[[3,3,3], [3,3,3], [3,3,3], [3,3,3], [3,3,3]], scale=[0.3, 0.3, 0.3], aux_loss= False)
    model.load_state_dict(torch.load(path)) 
 
    # Conversion to ONNX 
    Convert_ONNX()
